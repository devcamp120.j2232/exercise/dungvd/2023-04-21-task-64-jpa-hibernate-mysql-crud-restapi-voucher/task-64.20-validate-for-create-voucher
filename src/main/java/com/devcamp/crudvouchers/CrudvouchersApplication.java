package com.devcamp.crudvouchers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudvouchersApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudvouchersApplication.class, args);
	}

}
