package com.devcamp.crudvouchers.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.crudvouchers.model.CVoucher;
import com.devcamp.crudvouchers.repository.IVoucherRepository;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository pVoucherRepository;

    // API lấy thông tin toàn bộ vouchers
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            // List để chứa voucher
            List<CVoucher> listVouchers = new ArrayList<CVoucher>();
            // Thêm phần tử từ database vào list thông qua jpa repository
            pVoucherRepository.findAll().forEach(listVouchers::add);

            return new ResponseEntity<>(listVouchers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin 1 voucher thông qua ID
    @GetMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> getVoucherById(@PathVariable("id") long id) {
        try {
            // tạo mới biến voucher để tìm voucher thông qua id bởi hàm findById
            Optional<CVoucher> voucher = pVoucherRepository.findById(id);

            // Kiểm tra có tồn tại voucher hay không
            if (voucher.isPresent()) {
                // nếu có trả về voucher thông qua hàm.get() và status ok
                return new ResponseEntity<>(voucher.get(), HttpStatus.OK);
            } else {
                // Nếu không trả về null và status not Found
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            // Nếu gặp lỗi trả về null và status lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // API Tạo mới 1 voucher
    @PostMapping("/vouchers")
    public ResponseEntity<CVoucher> createVoucher(@Valid @RequestBody CVoucher pVoucher) {
        try {

            // Thêm ngày tạo mới và ngày cập nhật cho Object pVoucher
            pVoucher.setNgayTao(new Date());
            pVoucher.setNgayCapNhat(null);

            CVoucher _voucher = pVoucherRepository.save(pVoucher);
            return new ResponseEntity<>(_voucher, HttpStatus.OK);
        } catch (Exception e) {
            // Nếu gặp lỗi trả về null và status lỗi
            System.out.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API sửa một voucher thông qua id
    @PutMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> updateVoucher(@RequestBody CVoucher pVoucher,@PathVariable("id") long id) {
        try {
            // Tìm kiếm option voucher cần sửa thông qua Id bởi hàm findByID từ
            Optional<CVoucher> voucher = pVoucherRepository.findById(id);
            // Kiểm tra voucher có tồn tại hay không thông qua hàm isPresent()
            if (voucher.isPresent()) {

                // trả về kiểu dữ liệu voucher thông qua hàm.get();
                CVoucher voucherData = voucher.get();
                // Cập nhật lại các giá trị, trừ ID và ngày tạo
                voucherData.setMaVoucher(pVoucher.getMaVoucher());
                voucherData.setGhiChu(pVoucher.getGhiChu());
                voucherData.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
                // Set ngày câp nhật là ngày mới nhất
                voucherData.setNgayCapNhat(new Date());

                // Trả về voucher sau khi cập nhật
                return new ResponseEntity<>(pVoucherRepository.save(voucherData), HttpStatus.OK);
            } else {
                // Nếu không tìm thấy trả về null và status not found
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // Nếu gặp lỗi trả về null và thông báo lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API delete 1 voucher
    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> deleteVoucher(@PathVariable("id") long id) {
        try {
            pVoucherRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        catch (Exception e) {
            // Trả về null và status lỗi nếu gặp lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
